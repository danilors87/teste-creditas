# Descrição

Esse é um teste para a vaga de desenvolvedor frontend na creditas. O teste visa a criação de um Chat que possa escalar e ter e ter diversas funcionalidade. A descrição do desafio pode ser encontrada [aqui](https://github.com/Creditas/challenge/tree/master/frontend).
Na versão atual é possivel abrir vários chats, obter um historico de mensagens e enviar novas mensagens.

# Sobre o Front-end

O projeto está estruturado com o padrão MVC para garantir a separação de responsabilidades.

Stack utilizada:

- Webpack
- Babel
- ES6+
- Sass
- Karma
- Jasmime

# Sobre o Back-end

O back-end é um simples app em Json Server([JSON Server](https://github.com/typicode/json-server))
A app de back-end funciona apenas como simples simulador, nele é possivel obter uma lista de usúarios, listar o historico de mensagens e realizar o post de novas mensagens.

# endpoints:

    http://localhost:7000/users
    http://localhost:7000/messages

# Pré requisitos

    NodeJs 8.9.4 ou superior

# Instalação & Inicialização

    Backend
        server/npm install
        server/npm start
    Front-end
        creditas-front/npm install
        creditas-front/npm start

# Testes

    creditas-front/npm test
