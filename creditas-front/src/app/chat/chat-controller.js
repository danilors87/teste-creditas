import Message from './message';
import Constants from '../utils/constants';
import ChatService from '../services/chat-service';
export default class ChatController {
  constructor(chatView, chatModel) {
    this._chatView = chatView;
    this._chatModel = chatModel;
  }

  init() {
    this._chatView.sendMessageEvent = this.sendMessageEvent.bind(this);
    this._chatView.init();
  }

  async sendMessageEvent(e) {
    if (
      e.keyCode === Constants.KEYCODE.ON_ENTER ||
      e.type === Constants.MOUSE_EVENT.CLICK
    ) {
      const value = this._chatView.getInputValue();
      if (value && value !== '') {
        const message = new Message(value, new Date().getTime(), 'mine');
        try {
          const response = await ChatService.sendMessage(
            this._chatModel.id,
            message
          );
          if (response.id > 0) {
            this._chatModel.addMessage(message);
          }
        } catch (error) {
          console.log(error);
        }
      }
    }
  }
}
