import ChatView from './chat-view';
import ChatModel from './chat-model';
import ChatController from './chat-controller';
import query from '../utils/simple-query';
import ChatService from '../services/chat-service';
import Message from './message';
describe('Test Chat Controller', () => {
  let controller;
  let container;
  let chatView;
  let chatModel;
  beforeEach(() => {
    container = query('<div>');
    query('body').append(container);
    chatView = new ChatView('chatId', 'chatName', container, []);
    chatModel = new ChatModel('chatId');
    controller = new ChatController(chatView, chatModel);
  });

  afterEach(function() {
    const element = container.element;
    element.parentNode.removeChild(element);
    controller = null;
  });

  it('should call send message when event is enter keyUp', () => {
    controller.init();

    spyOn(ChatService, 'sendMessage').and.returnValue({ id: 10 });
    spyOn(chatView, 'getInputValue').and.returnValue('text message keyup');

    const onEnterEvent = { keyCode: 13 };
    controller.sendMessageEvent(onEnterEvent);
    expect(ChatService.sendMessage).toHaveBeenCalled();
  });

  it('should call send message when event is  mouse click', () => {
    controller.init();

    spyOn(ChatService, 'sendMessage').and.returnValue({ id: 12 });
    spyOn(chatView, 'getInputValue').and.returnValue('text message click');

    const onMouseClickEvent = { type: 'click' };
    controller.sendMessageEvent(onMouseClickEvent);
    expect(ChatService.sendMessage).toHaveBeenCalled();
  });
});
