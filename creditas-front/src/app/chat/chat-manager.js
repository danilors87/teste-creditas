import ChatController from './chat-controller';
import ChatService from '../services/chat-service';
import query from '../utils/simple-query';
import ChatView from './chat-view';
import ChatModel from './chat-model';
export default class ChatManager {
  constructor(chatId, chatName) {
    this._chatId = chatId;
    this._chatName = chatName;
    this._messageHistory = [];
  }

  async init() {
    if (query(`#chat-${this._chatId}`).hasElement()) {
      query(`#chat-${this._chatId}`).show();
      return;
    }

    this._messageHistory = await ChatService.getChatHistory(this._chatId);
    const chatContainer = query('.chat-container');
    const chatModel = new ChatModel(this._chatId);
    const chatView = new ChatView(
      this._chatId,
      this._chatName,
      chatContainer,
      this._messageHistory
    );

    const chatController = new ChatController(chatView, chatModel);
    chatController.init();
  }
}
