import PubSub from 'pubsub-js';

export default class ChatModel {
  constructor(id, messages = []) {
    this._id = id;
    this._messages = messages;
  }

  get id() {
    return this._id;
  }

  get messages() {
    return this._messages;
  }

  addMessage(message) {
    this._messages.push(message);
    PubSub.publish(`add-message-${this.id}`, message);
  }
}
