import ChatModel from '../chat/chat-model';
import Message from './message';

describe('Test Chat Model', () => {
  it('should add  message', () => {
    const chatModel = new ChatModel('chat1', []);

    expect(chatModel.messages.length).toBe(0);
    chatModel.addMessage(new Message('text', new Date().getTime(), 'mine'));
    expect(chatModel.messages.length).toBe(1);
  });
});
