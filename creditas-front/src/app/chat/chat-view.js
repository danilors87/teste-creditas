import query from '../utils/simple-query';
import { formattedHour } from '../utils/date-formatter';
import PubSub from 'pubsub-js';
export default class ChatView {
  constructor(chatId, chatName, element, messages = []) {
    this._element = element;
    this._chatId = chatId;
    this._chatName = chatName;
    this._messages = messages;
    this.sendMessageEvent = null;
    this.textValue = null;

    const _delegateMessage = this._delegateMessage.bind(this);
    PubSub.subscribe(`add-message-${this._chatId}`, _delegateMessage);
  }

  _delegateMessage(subscriber, message) {
    this._renderMessage(message);
    this.scrollDown();
    this.clearInput();
  }

  render() {
    const template = `<div id="chat-${this._chatId}" class="chat">
    <div class="chat-header">   
        <span>${this._chatName}</span>
        <a class="close-btn" href="javascript:void(0)">x</a>
    </div>
    <div class="chat-history">
        <ul class="message-list">
        </ul>
    </div>
        <div class="message-input">
            <input type="text" placeholder="text a message" />
            <button type="button">Enviar</button>
        </div>
    </div>`;

    this._element.append(template);
  }

  _cacheDOM() {
    this._input = query(`#chat-${this._chatId} .message-input input`);
    this._messageList = query(`#chat-${this._chatId} .message-list`);
    this._button = query(`#chat-${this._chatId}  .message-input button`);
    this._closeBtn = query(`#chat-${this._chatId} .close-btn`);
    this._chatHistory = query(`#chat-${this._chatId} .chat-history`);
  }

  _renderMessages() {
    this._messages.forEach(message => {
      this._messageList.append(this._renderMessage(message));
    });
  }

  _registerOnEnterEvent() {
    this._input.element.addEventListener('keyup', this.sendMessageEvent);
  }

  _registerOnclickButtonEvent() {
    this._button.element.addEventListener('click', this.sendMessageEvent);
  }

  _registerCloseEvent() {
    this._hideChat = this._hideChat.bind(this);
    this._closeBtn.element.addEventListener('click', this._hideChat);
  }

  _hideChat() {
    query(`#chat-${this._chatId}`).hide();
  }

  getInputValue() {
    return this._input.element.value;
  }

  clearInput() {
    this._input.element.value = '';
  }

  scrollDown() {
    const element = this._chatHistory.element;
    element.scrollTop = element.offsetHeight;
  }

  inputFocus() {
    this._input.element.focus();
  }

  _renderMessage(message) {
    this._messages.push(message);

    const textElement = query('<div>')
      .addClass(`text ${message.from}`)
      .append(message.text);

    const timeElement = query('<span>')
      .addClass('time')
      .append(formattedHour(message.time));
    const line = query('<li>')
      .append(timeElement)
      .append(textElement);
    this._messageList.append(line);
  }

  init() {
    this.render();
    this._cacheDOM();
    this._renderMessages();
    this._registerOnEnterEvent();
    this._registerOnclickButtonEvent();
    this._registerCloseEvent();
    this.inputFocus();
    this.scrollDown();
  }
}
