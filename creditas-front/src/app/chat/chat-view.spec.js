import ChatView from './chat-view';
import query from '../utils/simple-query';
import Message from './message';
describe('Test Chat View', () => {
  let chatView;
  let container;
  beforeEach(() => {
    const chatId = '119';
    const chatName = 'Chat 1';

    container = query('<div>');
    query('body').append(container);
    const history = [];

    chatView = new ChatView(chatId, chatName, container, history);
    chatView.sendMessageEvent = () => null;

    spyOn(chatView, 'sendMessageEvent');

    chatView.init();
  });

  afterEach(function() {
    const element = container.element;
    element.parentNode.removeChild(element);
  });

  it('should verify if chatview was initialized', () => {
    expect(chatView._input.element).toBeDefined();
    expect(chatView._messageList).toBeDefined();
    expect(chatView._input).toBeDefined();
  });

  it('should render message', () => {
    const text = 'message text';
    const time = new Date().getTime();
    const from = 'mine';

    expect(chatView._messages.length).toBe(0);

    chatView._renderMessage(new Message(text, time, from));

    expect(chatView._messages.length).toBe(1);
    expect(chatView._messageList.element.children.length).toBe(1);
  });

  it('should get input value', () => {
    chatView._input.element.value = 'my new text message';

    const value = chatView.getInputValue();

    expect(value).toBe('my new text message');
  });

  it('should get input value', () => {
    chatView._input.element.value = 'my new text message';

    const value = chatView.getInputValue();

    expect(value).toBe('my new text message');
  });

  it('should verify if on click event is called', () => {
    chatView._button.element.click();

    expect(chatView.sendMessageEvent).toHaveBeenCalled();
  });

  it('should verify if on enter event is called', () => {
    const ev = document.createEvent('Event');
    ev.initEvent('keyup', true, true);
    ev.keyCode = 13;
    ev.key = 'Enter';

    chatView._input.element.dispatchEvent(ev);

    expect(chatView.sendMessageEvent).toHaveBeenCalled();
  });

  it('should clear input value', () => {
    chatView._input.element.value = 'text value';

    expect(chatView.getInputValue()).toBe('text value');

    chatView.clearInput();

    expect(chatView.getInputValue()).toBe('');
  });
});
