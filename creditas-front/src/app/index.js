import './styles/reset.scss';
import './styles/style.scss';
import query from '../app/utils/simple-query';
import PanelController from '../app/panel/panel-controller';
import PanelModel from '../app/panel/panel-model';
import PanelView from '../app/panel/panel-view';

const chatsElement = query('.chats');

const panelView = new PanelView(chatsElement);
const panelModel = new PanelModel();
const controller = new PanelController(panelView, panelModel);

controller.init();
