import ChatManager from '../chat/chat-manager';

export default class PanelController {
  constructor(panelView, panelModel) {
    this._panelView = panelView;
    this._panelModel = panelModel;
  }

  async init() {
    this._panelView.openChat = this.openChat;

    this._panelView.users = await this._panelModel.getUsers();
    this._panelView.init();
  }

  openChat(e) {
    const chatId = e.target.dataset.chat;
    const chatName = e.target.text;
    const chatManager = new ChatManager(chatId, chatName);

    chatManager.init();
  }
}
