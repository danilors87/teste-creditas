import UserService from '../services/user-service';

export default class PanelModel {
  getUsers() {
    return UserService.getUsers();
  }
}
