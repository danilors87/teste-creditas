import query from '../utils/simple-query';

export default class PanelView {
  constructor(element) {
    this._element = element;
    this.openChat = null;
    this._users = [];
  }

  set users(users) {
    this._users = users;
  }

  render() {
    const template = `<ul>
     ${this._users
       .map(user => {
         return `<li><a id="link-${user.id}" data-chat="${
           user.chat_id
         }" href="javascript:void(0)">${user.name}</a></li>`;
       })
       .join('')}</ul>`;
    this._element.append(template);
  }

  _registerClickEvent() {
    this._users.forEach(user => {
      const link = query(`#link-${user.id}`);
      link.element.addEventListener('click', this.openChat);
    });
  }

  init() {
    this.render();
    this._registerClickEvent();
  }
}
