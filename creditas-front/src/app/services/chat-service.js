class ChatService {
  getChatHistory(chatId) {
    return fetch(`http://localhost:7000/messages?chat_id=${chatId}`).then(
      response => response.json()
    );
  }

  sendMessage(chatId, message) {
    const body = JSON.stringify(Object.assign({}, message, { chat_id: chatId }));
    return fetch('http://localhost:7000/messages', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: body
    }).then(response => response.json());
  }
}

export default new ChatService();
