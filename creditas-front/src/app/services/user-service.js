class UserService {
  getUsers() {
    return fetch(`http://localhost:7000/users`).then(response =>
      response.json()
    );
  }
}

export default new UserService();
