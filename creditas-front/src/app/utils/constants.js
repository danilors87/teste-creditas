const Constants = {
  KEYCODE: {
    ON_ENTER: 13
  },
  MOUSE_EVENT: {
    CLICK: 'click'
  }
};

export default Constants;
