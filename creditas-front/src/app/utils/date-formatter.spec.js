import { formattedHour } from './date-formatter';

describe('Test Date formatter', () => {
  beforeEach(() => {});

  it('should format date time value to hour', () => {
    const expectedHour = '8:58 am';
    const dateTime = '1531396720641';
    const hourFormatted = formattedHour(dateTime);
    expect(hourFormatted).toBe(expectedHour);
  });
});
