class SimpleQuery {
  constructor(element) {
    this._element = element;
  }

  static getElementById(id) {
    const element = document.getElementById(id);
    return new SimpleQuery(element);
  }

  static getElementByClass(className) {
    const elements = document.getElementsByClassName(className);
    if (elements.length > 0) {
      return new SimpleQuery(elements[0]);
    }
  }

  static createElement(tagName) {
    const element = document.createElement(tagName);
    return new SimpleQuery(element);
  }

  static queryElement(query) {
    const reg = /<\w*>/g;
    if (reg.test(query)) {
      const tagName = query.replace(/<|>/g, '');
      return SimpleQuery.createElement(tagName);
    }
    const element = document.querySelector(query);
    return new SimpleQuery(element);
  }

  get element() {
    return this._element;
  }

  hasElement() {
    return this._element !== null && this._element !== undefined;
  }

  addClass(className) {
    if (this._element) {
      const old = this._element.className;
      this._element.className = old.concat(' ' + className).trim();
    }
    return this;
  }

  append(content) {
    if (content instanceof SimpleQuery) {
      content = content.element;
    }
    if (content instanceof Element) {
      this._element.appendChild(content);
    }
    if (typeof content === 'string') {
      const element = document.createElement('div');
      element.innerHTML = content;
      this._element.appendChild(element);
    }
    return this;
  }

  hide() {
    this._element.style.display = 'none';
    return this;
  }

  show() {
    this._element.style.display = 'block';
    return this;
  }
}

const queryElement = SimpleQuery.queryElement;

export default queryElement;
