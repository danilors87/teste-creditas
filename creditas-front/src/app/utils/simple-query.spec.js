import query from './simple-query';
describe('Test Simple Query', () => {
  beforeEach(() => {});

  it('should be defined', () => {
    expect(query).toBeDefined();
  });

  it('should create element', () => {
    const div = query('<div>');
    expect(div).toBeDefined();
    expect(div.element.tagName).toBe('DIV');
  });

  it('should add classes to element', () => {
    const class1 = 'class1';
    const class2 = 'class2';
    const div = query('<div>');
    div.addClass(class1).addClass(class2);
    expect(div.element.className).toBe('class1 class2');
  });

  it('should add text html', () => {
    const textHtml = `<ul><li></li></ul>`;

    const div = query('<div>');
    div.append(textHtml);
    expect(div.element.innerHTML.indexOf(textHtml) > -1).toBeTruthy();
  });

  it('should add html element', () => {
    const htmlElement = query('<span>').element;

    const div = query('<div>');
    div.append(htmlElement);
    expect(div.element.innerHTML).toBe(htmlElement.outerHTML);
    expect(div.element).toBe(htmlElement.parentElement);
  });

  it('should have no element', () => {
    const newElement = query();
    expect(newElement.hasElement()).toBeFalsy();
  });

  it('should have element', () => {
    const newElement = query('<p>');
    expect(newElement.hasElement()).toBeTruthy();
  });

  it('should query element by id ', () => {
    const elementId = 'para-id-value';
    const paragraph = query('<p>').element;
    paragraph.id = elementId;
    document.body.appendChild(paragraph);

    const elementRecovered = query(`#${elementId}`).element;

    expect(elementRecovered.id).toBe(elementId);
  });

  it('should query element by class', () => {
    const className = 'para-class-value';
    const paragraph = query('<p>').addClass(className);

    query('body').append(paragraph);

    const elementQueried = query(`.${className}`).element;
    expect(elementQueried.className).toBe(className);
  });

  it('should query element by tag and class', () => {
    const className = 'para-class-value';
    const paragraph = query('<p>').addClass(className);

    query('body').append(paragraph);

    const elementQueried = query('p.para-class-value');

    expect(elementQueried.element.tagName).toBe('P');
    expect(elementQueried.element.className).toBe('para-class-value');
  });

  it('should hide element', () => {
    const div = query('<div>');
    expect(div.element.style.display === 'block').toBeFalsy();
    div.hide();
    expect(div.element.style.display === 'none').toBeTruthy();
  });

  it('should show element', () => {
    const div = query('<div>');

    expect(div.element.style.display === 'block').toBeFalsy();
    div.show();
    expect(div.element.style.display === 'block').toBeTruthy();
  });
});
